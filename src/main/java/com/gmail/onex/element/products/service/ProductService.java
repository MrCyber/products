package com.gmail.onex.element.products.service;

import com.gmail.onex.element.products.domain.ProductEntity;

import java.util.List;

public interface ProductService {

    List<ProductEntity> getAll();

    ProductEntity findByTitle(String name);
}
