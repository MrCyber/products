package com.gmail.onex.element.products.configuration;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.gmail.onex.element.products.repository")
public class DatabaseConfiguration {
}
