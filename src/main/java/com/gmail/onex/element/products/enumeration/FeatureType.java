package com.gmail.onex.element.products.enumeration;

public enum FeatureType {

    HDD,
    COLOR,
    WEIGHT

}
