package com.gmail.onex.element.products.service.impl;

import com.gmail.onex.element.products.domain.ProductEntity;
import com.gmail.onex.element.products.repository.ProductRepository;
import com.gmail.onex.element.products.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    @Transactional
    public List<ProductEntity> getAll() {
        return productRepository.findAll();
    }

    @Override
    public ProductEntity findByTitle(String name) {
        return productRepository.findByTitleIgnoreCase(name).orElse(null);
    }
}
