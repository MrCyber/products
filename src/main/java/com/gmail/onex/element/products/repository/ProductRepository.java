package com.gmail.onex.element.products.repository;

import com.gmail.onex.element.products.domain.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

    List<ProductEntity> findAll();

    Optional<ProductEntity> findByTitleIgnoreCase(String title);

}
