package com.gmail.onex.element.products;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.gmail.onex.element.products")
public class ProductsApplicationStarter {

    public static void main(String[] args) {
        SpringApplication.run(ProductsApplicationStarter.class, args);
    }

}
