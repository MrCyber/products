package com.gmail.onex.element.products.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.gmail.onex.element.products.enumeration.FeatureType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;


@Getter
@Setter
@Entity
@Table(name = "feature")
public class FeatureEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private FeatureType type;

    @Column(name = "value")
    private String value;

    @JsonBackReference
    @ManyToMany
    private Set<ProductEntity> products;

}
