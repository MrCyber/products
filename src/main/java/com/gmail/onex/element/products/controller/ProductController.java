package com.gmail.onex.element.products.controller;

import com.gmail.onex.element.products.domain.ProductEntity;
import com.gmail.onex.element.products.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController (ProductService productService){
        this.productService = productService;
    }

    @GetMapping("/products")
    public ResponseEntity<List<ProductEntity>> getAll() {
        return ResponseEntity.ok(productService.getAll());
    }

    @GetMapping("/product")
    public ResponseEntity<ProductEntity> findByTitle(@RequestParam("title") String  title) {
        return ResponseEntity.ok(productService.findByTitle(title));
    }
}
