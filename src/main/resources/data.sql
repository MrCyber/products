INSERT INTO product (id, title) VALUES (1, 'iPhone');
INSERT INTO product (id, title) VALUES (2, 'iPad');
INSERT INTO product (id, title) VALUES (3, 'iPod');

INSERT INTO feature (id, type, value) VALUES (1, 'COLOR', 'Space Grey');
INSERT INTO feature (id, type, value) VALUES (2, 'COLOR', 'Midnight Green');
INSERT INTO feature (id, type, value) VALUES (3, 'COLOR', 'Silver');
INSERT INTO feature (id, type, value) VALUES (4, 'HDD', '64');
INSERT INTO feature (id, type, value) VALUES (5, 'HDD', '128');
INSERT INTO feature (id, type, value) VALUES (6, 'HDD', '256');
INSERT INTO feature (id, type, value) VALUES (7, 'WEIGHT', '174');
INSERT INTO feature (id, type, value) VALUES (8, 'WEIGHT', '430');
INSERT INTO feature (id, type, value) VALUES (9, 'WEIGHT', '21.1');

INSERT INTO product_feature (product_id, feature_id) VALUES (1, 1);
INSERT INTO product_feature (product_id, feature_id) VALUES (1, 2);
INSERT INTO product_feature (product_id, feature_id) VALUES (1, 3);
INSERT INTO product_feature (product_id, feature_id) VALUES (1, 5);
INSERT INTO product_feature (product_id, feature_id) VALUES (1, 6);
INSERT INTO product_feature (product_id, feature_id) VALUES (1, 7);
INSERT INTO product_feature (product_id, feature_id) VALUES (2, 1);
INSERT INTO product_feature (product_id, feature_id) VALUES (2, 3);
INSERT INTO product_feature (product_id, feature_id) VALUES (2, 5);
INSERT INTO product_feature (product_id, feature_id) VALUES (2, 6);
INSERT INTO product_feature (product_id, feature_id) VALUES (2, 8);
INSERT INTO product_feature (product_id, feature_id) VALUES (3, 3);
INSERT INTO product_feature (product_id, feature_id) VALUES (3, 4);
INSERT INTO product_feature (product_id, feature_id) VALUES (3, 9);




